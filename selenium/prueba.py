from selenium import webdriver
import os
from selenium.webdriver.chrome.options import Options
from time import sleep

options = Options()
options.binary_location = os.environ.get("GOOGLE_CHROME_BIN")
options.add_argument("--headless")
options.add_argument("--disable-dev-shm-usage")
options.add_argument("--no-sandbox")
driver = webdriver.Chrome(options=options, executable_path=os.environ.get("CHROMEDRIVER_PATH"))

driver.get("https://www.google.com")
print(driver.page_source)
